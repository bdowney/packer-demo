package main

import (
	"fmt"

	"rsc.io/quote"
)

func Hello() string {
	return quote.Hello()
}

func main() {
	fmt.Println("hello world")
	username := "admin"
	var password = "f62e5bcda4fae4f82370da0c6f20697b8f8447ef"
	fmt.Println("Doing something with: ", username, password)
}
